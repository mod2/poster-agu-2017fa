\documentclass[final,mathserif,17pt]{beamer}


	\usepackage[orientation=landscape,size=a0,scale=1.4]{beamerposter}
	\mode<presentation>{\usetheme{UTexas}}
	\usepackage[utf8]{inputenc}
	\usepackage[english]{babel}
	\usepackage{hyperref}
	\usepackage{ragged2e}
	\usepackage[font=scriptsize,labelfont=bf,justification=justified]{caption}
	\usepackage{array,booktabs,tabularx}

	\usepackage[absolute,overlay]{textpos}

	\usecolortheme{beaver}
	\beamertemplatenavigationsymbolsempty
	\setbeamerfont{title}{family=\rm}

%	\usepackage{fullpage}
	\usepackage{graphicx}
	\usepackage[numbers]{natbib}
%	\usepackage[linktoc=all,backref]{hyperref}
%	\usepackage{sectsty}
%	\usepackage[raggedright]{titlesec}
%	\usepackage[font=small,labelfont=bf,justification=centering]{caption}
%	\usepackage{listings}
%	\usepackage{color}
%	\usepackage{alltt}
%	\usepackage{bm}
%	\usepackage{multicol}

	\renewcommand*\rmdefault{ptm}
%	\renewcommand*\bibname{Literature Cited}
%	\setcounter{tocdepth}{3}
%	\setcounter{secnumdepth}{3}
	\setlength{\columnseprule}{1pt}
	\setlength{\columnsep}{13pt}

%	\newcommand*{\ParFlow}{\textsc{ParFlow}}
%	\newcommand*{\MODFLOW}{\textsc{modflow}}
%	\newcommand*{\mfnstotk}{\textsc{mf96to2k}}
%	\newcommand*{\mftktoof}{\textsc{mf2kto05}}
%	\newcommand*{\FloPy}{\textsc{FloPy}}
%	\newcommand*{\FloPerl}{\textsc{FloPerl}}
%	\newcommand*{\Tcl}{\textsc{Tcl}}

%	\titleformat{\chapter}[display]
%		{\centering\huge\bfseries}{\thechapter}{1em}{\huge}
%	\chapterfont{\centering}
%	\raggedbottom
%	\widowpenalties 1 10000


%	\title[Interoperability of Flow Models]%
	\title{\Huge From MODFLOW-96 to MODFLOW-2005, ParFlow, and Others}
%	\subtitle{Updates and a Workflow for Up- and Out-Conversion}
%	\author[Hardesty Lewis, Pierce]%
	\author{\Large Daniel Hardesty Lewis$^{1}$ \and Suzanne A. Pierce$^{2}$}
	\institute[UT-Austin]{$^{1}$Department of Mathematics, University of Texas at Austin \\ $^{2}$Texas Advanced Computing Center, University of Texas at Austin}
%	\date[AGU 2017 Fall]%
	\date{AGU Fall Meeting, 2017}
%	\subject{Earth and space science informatics}

	\newlength{\columnheight}
	\setlength{\columnheight}{150cm}


\begin{document}


%	{\huge\bfseries From MODFLOW-96 to MODFLOW-2005, ParFlow, and Others:\par}
%	{\LARGE\bfseries Updates and a Workflow for Up- and
%		Out-Conversion\footnote{
%			\footnotesize\centering
%			\setlength{\parskip}{0.5\baselineskip}
%			Copyright \copyright 2017, Daniel Hardesty~Lewis\par
%			All rights reserved.\par
%			Redistribution and use of this computer code in source and binary
%				forms, with or without modification, are permitted provided
%				that the conditions of the {M}odified~{BSD}~{L}icense are
%				met.\par
%			Permission is granted to copy, distribute, and\slash or modify this
%				document under the terms of the {C}reative~{C}ommons
%				{A}ttribution 4.0 {I}nternational {P}ublic~{L}icense.
%		}\par}
%	{\Large\itshape Daniel Hardesty~Lewis\par}

	\begin{frame}
	\begin{columns}
%		\raggedright
		\begin{column}{.28\textwidth}
			\begin{beamercolorbox}[center]{postercolumn}
				\begin{minipage}{.98\textwidth}
					\parbox[t][\columnheight]{\textwidth}{
					\begin{myblock}{Introduction}

		
		
%		ᴍᴏᴅғʟᴏᴡ (ᴍғ) has served for decades as a de facto standard for groundwater modelling. 
%		Despite successive versions, legacy ᴍғ-96 simulations are still commonly encountered cases. 
%		Such is the case for many of the groundwater availability models of the State of Texas. 
%		Unfortunately, even the existence of converters to ᴍғ's newer versions has not necessarily 
%		stimulated their adoption, let alone re-creation of legacy models. 
%		This state of affairs may be due to the unfamiliarity of the modeller with the terminal 
%		or the Fᴏʀᴛʀᴀɴ programming language, resulting in an inability to address the minor or major bugs, 
%		nuances, or limitations in compilation or execution of the conversion programs. Here, we present a 
%		workflow that addresses the above intricacies all the while attempting to maintain portability 
%		in implementation.
%
%		This workflow is contructed in the form of a Bash script and - with the geoscience-oriented in mind -
%		represented as a Jupyter notebook. First, one may choose whether this executable will run with 
%		ᴘᴏsɪx-compliance or with a preference towards the Bash facilities, both widely adopted by operating 
%		systems. In the same vein, it attempts to function within minimal command environments, which reduces
%		any dependencies. Finally, it is designed to offer parallelism across as many cores and nodes as 
%		necessary or as few as desired, whether upon a personal or supercomputer.

%		Underlying this workflow are patches such that the legacy MF-96 models may be compiled and executed upon 
%		modern hardware. Also, fixes to long-standing bugs and limitations in the existing ᴍғ converters have been 
%		prepared. Specifically, support for the conversion of -96- and Horizontal Flow Barrier-coupled simulations 
%		has been added. More radically, we have laid the foundations of a conversion utility between ᴍғ and a 
%		similar modeller, ParFlow. Furthermore, the modular approach followed may extend to an application which 
%		inter-operates between arbitrary groundwater simulators.

%		In short, an accessible and portable workflow of the process of up-conversion between ᴍᴏᴅғʟᴏᴡ versions now 
%		avails itself to geoscientists. Updated programs within it may allow for re-use, in whole or in part, legacy 
%		simulations. Lastly, a generic inter-operator has been established, invoking the possibility of significant 
%		ease in the recycling of groundwater data in the future.
%		
%		Legacy MF-96 groundwater models still stand as \textit{de facto}
%			standards in use widely from the {S}tate of {T}exas and
%			elsewhere~\cite{harbaugh96}.
%		The use of MF-96 simulation models continues despite
%			the existence of up-to-date versions of MODFLOW and newer groundwater 
%			simulation codes like MF-2000 and MF-2005.
%		As computing tools and capabilities advance, it is particularly
%			difficult to address the minor or major bugs, nuances, or
%			limitations in compilation, execution, and extension of the legacy MF-96 code to construct 
%		 groundwater simulation applications software that leverage advanced computing capabilities.
%		We present here an accessible, reusable, and portable workflow which
%			converts MF-96 version models to later versions, as well the beginnings of an interchanger
%			amongst the data formats of MODFLOW and arbitrary other groundwater
%			flow simulators.
%		Unfortunately, even the existence of conversion utilities amongst the
%			differing versions of MODFLOW has not necessarily stimulated the
%			adoption of the more modern versions, let alone the re-creation of
%			equivalent models from them~\cite{harbaugh96}.
%		Here, we present a workflow which captures the above intricacies all
%			the while attempting to maintain a great portability in
%			implementation.%\footnote{The work, including this paper, discussed
%			hereafter is recorded by the Git projects grouped under the
%			following:\par
%			\url{https://gitlab.com/mod2}}

					\end{myblock}
					\begin{myblock}{Workflow}
	
%		This workflow is constructed in the form of a Bash~script and -- with
%			those more oriented towards the geosciences in mind -- represented
%			as a Jupyter notebook, allowing the lay-programmer to interact with
%			the derivation of results as they are
%			coded~\cite{gnu16}~\cite{jupyter17}.
%		Within the header of the program, one is given a switch with which to
%			choose whether it will operate with compliance with the Portable
%			Operating~System Interface (POSIX) standards or with a preference
%			towards the more common Bash scripting facilities~\cite{austin16},
			both widely adopted amongst many UNIX(-like) operating systems.
		In a similar vein, an attempt is made to function within
			operating~systems lacking typical shell commands, the very act of
			which reduces dependencies prerequisite to use of the workflow.
		Such features extend the re-usability of this program.
		As well, the workflow is designed to offer parallelisation across as
			many cores and nodes as necessary or as few as desired, whether
			upon a personal or super-computer.
	
					\end{myblock}
				}\end{minipage}\end{beamercolorbox}
		\end{column}
		\begin{column}{.28\textwidth}
			\begin{beamercolorbox}[center]{postercolumn}
				\begin{minipage}{.98\textwidth}
					\parbox[t][\columnheight]{\textwidth}{
					\begin{myblock}{Methods}

	\begin{figure}
		\centering
		\begin{minipage}{0.45\columnwidth}
			\centering
			\includegraphics[width=0.8\columnwidth]{Peckham-Point_Point.png}
			\caption{$O(n^2)$ -- an exemplary, fully connected
				point-to-point network}
			\label{fig:p2p}
		\end{minipage}\hfill
		\begin{minipage}{0.45\columnwidth}
			\centering
			\includegraphics[width=0.9\columnwidth]{Peckham-Spoke_Hub.png}
			\caption{$O(n)$ -- a spoke-hub network with a single central
				node}
			\label{fig:sh}
		\end{minipage}
	\end{figure}
		Underlying this workflow are some programs updated such that they may
			now compile and execute upon modern hardware.
		Also, fixes to long-standing bugs and limitations in the existing
			MODFLOW conversion utilities have been prepared.
		Specifically, support for any up-conversion of -96 simulations that
			were run in conjunction with the {H}orizontal~{F}low {B}arrier
			package has been added~\cite{hsieh93}.
		Even further afield, a conversion utility between MODFLOW and the
			groundwater modelling software, ParFlow, has been
			founded~\cite{maxwell16}.
		Put more technically, a mapping between the data of the input files of
			MODFLOW and that of ParFlow has been begun.
		As the figures depict in their contrast, rather than directly map each
			point, representing a modelling application, to another, the 
			ontology, {G}eoscience {S}tandard~{N}ames, has been selected as a
			hub through which to most efficiently pass data~\cite{peckham14}.
		Efficiency is here induced by modularity, itself a gain.
%		Underlying this workflow are some programs in which updates have been
%			written such that they may compile and execute upon modern
%			hardware.
%		Also, fixes to long-standing bugs and limitations in the existing
%			MODFLOW conversion utilities have been prepared.
%		Specifically, support for any up-conversion of -96 simulations that
%			were run in conjunction with the {H}orizontal~{F}low {B}arrier
%			package has been added~\cite{hsieh93}.
%		Even further afield, a conversion utility between MODFLOW and the
%			groundwater modelling software, ParFlow, has been
%			created~\cite{maxwell16}.
%		The modular approach followed in crafting this converter is such that
%			it may be extended to a general-purpose application which
%			interoperates between arbitrary groundwater modellers.
%		As the figures depict in their contrast, such a program should target
%			each converter to the ontology, {G}eoscience {S}tandard~{N}ames,
%			to most efficiently pass data amongst differing simulation
%			codes~\cite{peckham14}.
%			\footnote{The illustrations on the following page display both the
%				improvement in portability and the reduction in complexity such
%				a model promotes.}

					\end{myblock}
					\begin{myblock}{Concluding remarks}

		In short, an accessible and portable workflow of the process of
			up-conversion between MODFLOW's versions becomes available.
		Certain components of it expect to find applicability in the re-use of
			legacy but essential simulations.
		Lastly, a generic inter-operator has been established, which, subject
			to further development, promises to significantly ease the
			recycling of groundwater data into the simulation codes of the
			future.
%		In short, an accessible and portable workflow of the process of
%			up-conversion between MODFLOW versions now avails itself to
%			geoscientists.
%		Some components of it may find applicability in the re-use of legacy
%			simulations.
%		Lastly, a generic inter-operator has been established, invoking the
%			possibility of significant ease in the recycling of groundwater
%			data in the future.

					\end{myblock}
				}\end{minipage}\end{beamercolorbox}
		\end{column}
		\begin{column}{.28\textwidth}
			\begin{beamercolorbox}[center]{postercolumn}
				\begin{minipage}{.98\textwidth}
					\parbox[t][\columnheight]{\textwidth}{
					\begin{myblock}{Acknowledgments}

		Collaboration with TACC and research for support for this project was provided  as 
			a fellowship received from the
			\textit{{T}exas~{I}nstitute for {D}iscovery~{E}ducation in {S}cience}
			of the {C}ollege of {N}atural~{S}ciences of the
			{U}niversity of {T}exas at {A}ustin.
		To the donors whose kind~hearts replenish it and the administrators
			whose fastidiousness maintains it, I am extraordinary grateful to
			you all.
		Lastly, no work of mine could have been possible had it not been for
			those of \textit{{N}ational~{S}cience {F}oundation} who
			have shared in the vision of {IS}-{GEO} and magnanimously funded
			this {R}esearch~{C}oordination {N}etwork with award 1633221.

					\end{myblock}
					\begin{myblock}{Bibliography}

	\scriptsize
	\let\Origclearpage\clearpage
	\let\clearpage\relax
	\bibliographystyle{plainnat}
	\bibliography{./sds-csc-paper.bib}
	\let\clearpage\Origclearpage

					\end{myblock}

				}\end{minipage}\end{beamercolorbox}
		\end{column}
	\end{columns}
	\end{frame}

\end{document}
