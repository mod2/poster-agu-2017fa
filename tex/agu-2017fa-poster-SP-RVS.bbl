\begin{thebibliography}{8}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[Avila et~al.(2017)Avila, Bussonier, Corlay, Frederic, Granger, Grout,
  Hamrick, Ivanov, Kluyver, Kelley, Parente, Perez, Ragan-Kelley, Silvester,
  and Willing]{jupyter17}
Damian Avila, Matthias Bussonier, Sylvain Corlay, Jonathan Frederic, Brian
  Granger, Jason Grout, Jessica Hamrick, Paul Ivanov, Thomas Kluyver, Kyle
  Kelley, Peter Parente, Fernando Perez, Min Ragan-Kelley, Steven Silvester,
  and Carol Willing.
\newblock {J}upyter documentation.
\newblock Version, {N}um{FOCUS} {F}oundation, Austin, United~States, July 2017.
\newblock URL \url{https://jupyter.readthedocs.io/en/latest/index.html}.

\bibitem[Harbaugh and McDonald(1996)]{harbaugh96}
Arlen~W. Harbaugh and Michael~G. McDonald.
\newblock \emph{User's~Documentation for {MODFLOW}-96, an update to the
  {U}.{S}.~{G}eological~{S}urvey Modular Finite-Difference Ground-Water Flow
  Model}.
\newblock {U}.{S}. {G}eological {S}urvey, Reston, United~States, 1996.
\newblock URL
  \url{https://water.usgs.gov/software/MODFLOW/code/doc/ofr96485.pdf}.

\bibitem[Hsieh and Freckleton(1993)]{hsieh93}
Paul~A. Hsieh and John~R. Freckleton.
\newblock \emph{Documentation of a Computer~Program to Simulate
  Horizontal-Flow~Barriers Using the {U}.{S}.~{G}eological~{S}urvey's Modular
  Three-Dimensional Finite-Difference Ground-Water Flow Model}.
\newblock {U}.{S}. {G}eological {S}urvey, Sacramento, United~States, 1993.
\newblock URL \url{https://pubs.usgs.gov/of/1992/0477/report.pdf}.

\bibitem[Josey and Fox(2016)]{austin16}
Andrew Josey and Cathy Fox.
\newblock Standard for information technology -- {P}ortable
  {O}perating~{S}ystem {I}nterface ({POSIX}).
\newblock Standard 1003.1, {A}ustin {G}roup, New~York, United~States and
  Reading, United~Kingdom, 2016.
\newblock URL
  \url{http://pubs.opengroup.org/onlinepubs/9699919799.2016edition/nframe.html}.

\bibitem[Maxwell et~al.(2016)Maxwell, Kollet, Smith, Woodward, Falgout,
  Ferguson, Engdahl, Condon, Hector, Lopez, Gilbert, Bearup, Jefferson,
  Collins, de~Graaf, Pribulick, Baldwin, Bosl, Hornung, and Ashby]{maxwell16}
Reed~M. Maxwell, Stefan~J. Kollet, Steven~G. Smith, Carol~S. Woodward,
  Robert~D. Falgout, Ian~M. Ferguson, Nicholas Engdahl, Laura~E. Condon, Basile
  Hector, Sonya Lopez, James Gilbert, Lindsay Bearup, Jennifer Jefferson,
  Caitlin Collins, Inge de~Graaf, Christine Pribulick, Chuck Baldwin,
  William~J. Bosl, Richard Hornung, and Steven Ashby.
\newblock \emph{{P}ar{F}low User's~Manual}.
\newblock {I}ntegrated {G}round{W}ater {M}odeling {C}enter, Golden,
  United~States, v.743 edition, February 2016.
\newblock URL
  \url{http://inside.mines.edu/~rmaxwell/parflow.manual.2-15-16.pdf}.

\bibitem[Peckham(2014)]{peckham14}
Scott~D. Peckham.
\newblock The {CSDMS}~{S}tandard~{N}ames: Cross-domain naming conventions for
  describing process~models, data~sets, and their associated~variables.
\newblock In D.~P. Ames, N.~W.~T. Quinn, and A.~E. Rizzoli, editors,
  \emph{Proceedings of the 7th~{I}ntl.~{C}ongress on {E}nv.~{M}odelling and
  {S}oftware}, San~Diego, United~States, June 2014. {I}nternational
  {E}nvironmental~{M}odelling \& {S}oftware {S}ociety ({iEMSs}).
\newblock URL \url{http://scholarsarchive.byu.edu/iemssconference/2014}.

\bibitem[Ramey(2016)]{gnu16}
Chet Ramey.
\newblock The {GNU} {B}ash reference~manual.
\newblock Version 4.4, {F}ree~{S}oftware {F}oundation, Boston, United~States,
  September 2016.
\newblock URL \url{https://www.gnu.org/software/bash/manual/bash.txt}.

\bibitem[{T}exas {W}ater~{D}evelopment {B}oard(2017)]{twdb17}
{T}exas {W}ater~{D}evelopment {B}oard.
\newblock {G}roundwater {A}vailability {M}odels.
\newblock Online, August 2017.
\newblock URL \url{http://www.twdb.texas.gov/groundwater/models/gam/index.asp}.

\end{thebibliography}
