\documentclass[final,14pt]{beamer}


	\usepackage[orientation=landscape,size=a0,scale=1.4]{beamerposter}
	\mode<presentation>{\usetheme{UTexas}}
	\usepackage[utf8]{inputenc}
	\usepackage[english]{babel}
	\usepackage{hyperref}
	\usepackage{ragged2e}
	\usepackage[font=scriptsize,labelfont=bf,justification=justified]{caption}
	\usepackage{array,booktabs,tabularx}
	\usepackage{graphicx}
	\usepackage[numbers]{natbib}
	\usepackage{textcomp}

	\usecolortheme{beaver}
	\beamertemplatenavigationsymbolsempty
	\setbeamerfont{title}{family=\rm}

	\renewcommand*\rmdefault{ptm}
	\setlength{\columnseprule}{1pt}
	\setlength{\columnsep}{13pt}


	\title{\Huge From MODFLOW-96 to MODFLOW-2005, ParFlow, and Others}
	\author{\Large Daniel Hardesty Lewis$^{1}$ \and Suzanne A. Pierce$^{2}$}
	\institute[UT-Austin]{$^{1}$Department of Mathematics, University of Texas at Austin \\ $^{2}$Texas Advanced Computing Center, University of Texas at Austin}
	\date{AGU Fall Meeting, 2017}

	\newlength{\columnheight}
	\setlength{\columnheight}{150cm}


\begin{document}


%	{\huge\bfseries From MODFLOW-96 to MODFLOW-2005, ParFlow, and Others:\par}
%	{\LARGE\bfseries Updates and a Workflow for Up- and
%		Out-Conversion\footnote{
%			\footnotesize\centering
%			\setlength{\parskip}{0.5\baselineskip}
%			Copyright \copyright 2017, Daniel Hardesty~Lewis\par
%			All rights reserved.\par
%			Redistribution and use of this computer code in source and binary
%				forms, with or without modification, are permitted provided
%				that the conditions of the {M}odified~{BSD}~{L}icense are
%				met.\par
%			Permission is granted to copy, distribute, and\slash or modify this
%				document under the terms of the {C}reative~{C}ommons
%				{A}ttribution 4.0 {I}nternational {P}ublic~{L}icense.
%		}\par}
%	{\Large\itshape Daniel Hardesty~Lewis\par}

	\begin{frame}

	\begin{columns}

		\begin{column}{.32\textwidth}

			\begin{beamercolorbox}[center]{postercolumn}
				\begin{minipage}{.98\textwidth}
					\parbox[t][\columnheight]{\textwidth}{

					\begin{myblock}{Introduction}
		
		MODFLOW (MF) has served for decades as a \textit{de facto} standard for
			groundwater modelling~\cite{harbaugh96}.
		Despite successive versions, legacy MF-96 simulations are still
			commonly encountered. 
		Such is the case for many of the groundwater availability models of the
			{S}tate of {T}exas~\cite{twdb17}.
		Unfortunately, even the existence of converters to MF's newer versions
			(MF-2000 and MF-2005) had not necessarily stimulated their
			adoption, let alone re-creation of legacy models. 
		With the still further advancement of computing tools and capabilities,
			it has become increasingly difficult to address the minor or major
			bugs, nuances, or limitations in compilation, execution, and
			extension of the legacy MF-96 code to construct
			groundwater~simulation applications that leverage
			advanced~computing capabilities.
		Here, we present an accessible, reusable, and portable workflow which
			converts models in MF-96 to more modern versions, as well as the
			beginnings of an interchanger amongst arbitrary versions of MODFLOW
			and arbitrary other groundwater flow simulators.

					\end{myblock}

					\begin{myblock}{Methods}

		Underlying this workflow are patches such that the legacy MF-96 models
			may be compiled and executed upon modern hardware.
		Also, fixes to long-standing bugs and limitations in the existing
			converters have been prepared.
		Specifically, support for the up-conversion of -96- and
			Horizontal~Flow~Barrier-coupled simulations has been
			added~\cite{hsieh93}.
		Figure~\ref{fig:wf} encapsulates these modifications, along with all
			others developed alongside this project.
		Even further afield, conversion utilities between the input data of MF
			and those of the groundwater~modelling software, ParFlow, have been
			founded~\cite{maxwell16}.
		Rather than directly map each application to each other, as depicted in
			Figure~\ref{fig:p2p}, the computationally most efficient manner to
			interchange data amongst modelling applications is through a
			central hub, as in Figure~\ref{fig:sh}.
		In this case, the ontology, {G}eoscience~{S}tandard {N}ames, has been
			selected as the intermediary against the differing softwares'
			namings of input variables~\cite{peckham14}.
		Refer once again to Figure~\ref{fig:wf} for a detail of
			Figure~\ref{fig:sh}
			summarising this particular conversion between MODFLOW and ParFlow.
		Envisioned and begun here is a modular and general interoperator
			amongst groundwater simulators.

					\end{myblock}

				}\end{minipage}\end{beamercolorbox}

		\end{column}

		\begin{column}{.32\textwidth}

			\begin{beamercolorbox}[center]{postercolumn}
				\begin{minipage}{.98\textwidth}
					\parbox[t][\columnheight]{\textwidth}{

					\begin{myblock}{Figures}

		\begin{figure}
			\centering
			\begin{minipage}{0.40\columnwidth}
				\centering
				\includegraphics[width=0.8\columnwidth]{Peckham-Point_Point.png}
				\caption{$O(n^2)$ -- an exemplary, fully connected
					point-to-point network}
				\label{fig:p2p}
			\end{minipage}\hfill
			\begin{minipage}{0.40\columnwidth}
				\centering
				\includegraphics[width=0.9\columnwidth]{Peckham-Spoke_Hub.png}
				\caption{$O(n)$ -- a spoke-hub network with a single central
					node}
				\label{fig:sh}
			\end{minipage}
		\end{figure}
		\begin{figure}
			\centering
			\begin{minipage}{.90\columnwidth}
				\centering
				\includegraphics[width=0.8\columnwidth]{workflow-RVS.png}
				\caption{Modifications}
				\label{fig:wf}
			\end{minipage}
		\end{figure}

					\end{myblock}

					\begin{myblock}{Design principles}
	
		This workflow is constructed in the form of a Bash script and -- with
			the geoscience-oriented in mind -- re-presented as a Jupyter
			notebook~\cite{gnu16}~\cite{jupyter17}.
		One may choose whether this executable will run with a preference
			towards POSIX-compliance or the use Bash facilities, both
			acting as standards across UNIX-like
			operating~systems~\cite{austin16}.
		In a similar vein, it attempts to function within minimal command
			environments, in order to reduce dependencies to a great degree.
		Finally, the workflow is designed to offer parallelism across as many
			cores and nodes as necessary or as few as desired, whether upon a
			personal or super-computer.
	
					\end{myblock}

				}\end{minipage}\end{beamercolorbox}

		\end{column}

		\begin{column}{.32\textwidth}

			\begin{beamercolorbox}[center]{postercolumn}
				\begin{minipage}{.98\textwidth}
					\parbox[t][\columnheight]{\textwidth}{

					\begin{myblock}{Concluding remarks}

		In short, an accessible and portable workflow of the process of
			up-conversion between MODFLOW's versions is now available to
			geoscientists.
		Updated programs within it may allow for re-use, in whole or in part,
			legacy simulations.
		Lastly, a generic inter-operator has been created, promising to
			significantly ease the recycling of groundwater data into the
			simulation~codes of the future.

					\end{myblock}

					\begin{myblock}{Acknowledgments}

%		\scriptsize
		Collaboration with the Texas Advanced Computing Center and research for
			support for this project was provided as a fellowship received from
			the
			{T}exas~{I}nstitute for {D}iscovery~{E}ducation in {S}cience
			of the {C}ollege of {N}atural~{S}ciences of the {U}niversity of
			{T}exas at {A}ustin.
		To the donors whose kind~hearts replenish it and the administrators
			whose fastidiousness maintains it, I am extraordinary grateful to
			you all.
		Lastly, no work of mine could have been possible had it not been for
			those of {N}ational~{S}cience {F}oundation who have shared in the
			vision of the {R}esearch~{C}oordination {N}etwork,
			{I}ntelligent~{S}ystems for {G}eosciences, and magnanimously funded
			award \textnumero 1633221.

					\end{myblock}

					\begin{myblock}{Bibliography}

%		\let\Origclearpage\clearpage
%		\let\clearpage\relax
		\scriptsize
		\bibliographystyle{plainnat}
		\bibliography{./sds-csc-paper.bib}
%		\let\clearpage\Origclearpage

					\end{myblock}

				}\end{minipage}\end{beamercolorbox}

		\end{column}

	\end{columns}

	\end{frame}

\end{document}
