
		The legacy groundwater modeller, MODFLOW-96, as well as those
			simulations derived from it, still stand as \textit{de facto}
			standards in use widely from the {S}tate of {T}exas to
			beyond~\cite{harbaugh96}.
		For a certain community of hydrogeologists, their use continues despite
			the existence of up-to-date versions of MODFLOW or even more
			appropriate and newer groundwater simulation codes.
		The research coordination network, Intelligent Systems for Geosciences,
			recognises the cause for such an inertia as the historical lack of
			familiarity of the interested geoscientist with the command line
			and the Fortran programming language (among
			others)~\cite{isgeo17}~\cite{ansi78}.
		Without acquaintance with such computing tools, it is particularly
			difficult to address the minor or major bugs, nuances, or
			limitations in compilation, execution, and extension of groundwater
			simulation software.
		We present here an accessible, re-usable, and portable workflow which
			converts upwards the input data of simulations in MODFLOW-96 to
			successive versions, as well the beginnings of an interchanger
			amongst the data formats of MODFLOW and arbitrary other groundwater
			flow simulators.
%		Despite supersedure by a number of its own versions, as well as other,
%			newer groundwater modelling softwares, MODFLOW-96 and its legacy
%			simulations remain as \textit{de facto} standards within much of
%			the hydrogeological community within the {S}tate of {T}exas and
%			elsewhere.
%		Unfortunately, even the existence of conversion utilities amongst the
%			differing versions of MODFLOW has not necessarily stimulated the
%			adoption of the more modern versions, let alone the re-creation of
%			equivalent models from them~\cite{harbaugh96}.
%		The primary reason for such a lack of adoption stands as the
%			unfamiliarity of the modeller with command line or the Fortran
%			programming language, resulting in an inability to address either
%			the minor or major bugs, nuances, or limitations in compilation or
%			execution of the conversion programs~\cite{ansi78}.
%		Here, we present a workflow which captures the above intricacies all
%			the while attempting to maintain a great portability in
%			implementation.%\footnote{The work, including this paper, discussed
%			hereafter is recorded by the Git projects grouped under the
%			following:\par
%			\url{https://gitlab.com/mod2}}

		This workflow is constructed in the form of a Bash~script and -- with
			those more oriented towards the geosciences in mind -- re-presented
			as a Jupyter notebook, allowing the lay-programmer to interact with
			the derivation of results as they are
			coded~\cite{gnu16}~\cite{jupyter17}.
		Within the header of the program, one is given a switch with which to
			choose whether it will operate with compliance with the Portable
			Operating~System Interface (POSIX) standards or with a preference
			towards the more common Bash scripting facilities~\cite{austin16},
			both widely adopted amongst many UNIX(-like) operating systems.
		In a similar vein, an attempt is made to function within
			operating~systems lacking typical shell commands, the very act of
			which reduces dependencies prerequisite to use of the workflow.
		Such features extend the re-usability of this program.
		As well, the workflow is designed to offer parallelisation across as
			many cores and nodes as necessary or as few as desired, whether
			upon a personal or super-computer.
%		This workflow is constructed in the form of a Bash script and -- with
%			those more oriented towards the geosciences in mind -- re-presented
%			as a Jupyter notebook, allowing the lay-programmer to interact with
%			the derivation of results as they are
%			coded~\cite{gnu16}~\cite{jupyter17}.
%		One may choose whether the program will operate with compliance with
%			the Portable Operating System Interface (POSIX) standards or with a
%			preference towards the more common Bash scripting
%			facilities~\cite{austin16}.
%		Both strongly extend the re-usability of this program as both are
%			widely adopted amongst many UNIX(-like) and other
%			operating~systems.
%		In the same vein, an attempt is made to function within
%			operating~systems lacking typical shell commands, the very act of
%			which reduces dependencies prerequisite to use of the workflow.
%		As well, the workflow is designed to offer parallelisation across as
%			many cores and nodes as necessary or as few as desired, whether
%			upon a personal or super-computer.
	
		Underlying this workflow are some programs updated such that they may
			now compile and execute upon modern hardware.
		Also, fixes to long-standing bugs and limitations in the existing
			MODFLOW conversion utilities have been prepared.
		Specifically, support for any up-conversion of -96 simulations that
			were run in conjunction with the {H}orizontal~{F}low {B}arrier
			package has been added~\cite{hsieh93}.
		Even further afield, a conversion utility between MODFLOW and the
			groundwater modelling software, ParFlow, has been
			founded~\cite{maxwell16}.
		Put more technically, a mapping between the data of the input files of
			MODFLOW and that of ParFlow has been begun.
		As the figures depict in their contrast, rather than directly map each
			point, representing a modelling application, to another, the 
			ontology, {G}eoscience {S}tandard~{N}ames, has been selected as a
			hub through which to most efficiently pass data~\cite{peckham14}.
		Efficiency is here induced by modularity, itself a gain.
%		Underlying this workflow are some programs in which updates have been
%			written such that they may compile and execute upon modern
%			hardware.
%		Also, fixes to long-standing bugs and limitations in the existing
%			MODFLOW conversion utilities have been prepared.
%		Specifically, support for any up-conversion of -96 simulations that
%			were run in conjunction with the {H}orizontal~{F}low {B}arrier
%			package has been added~\cite{hsieh93}.
%		Even further afield, a conversion utility between MODFLOW and the
%			groundwater modelling software, ParFlow, has been
%			created~\cite{maxwell16}.
%		The modular approach followed in crafting this converter is such that
%			it may be extended to a general-purpose application which
%			interoperates between arbitrary groundwater modellers.
%		As the figures depict in their contrast, such a program should target
%			each converter to the ontology, {G}eoscience {S}tandard~{N}ames,
%			to most efficiently pass data amongst differing simulation
%			codes~\cite{peckham14}.
%			\footnote{The illustrations on the following page display both the
%				improvement in portability and the reduction in complexity such
%				a model promotes.}

		In short, an accessible and portable workflow of the process of
			up-conversion between MODFLOW's versions becomes available.
		Certain components of it expect to find applicability in the re-use of
			legacy but essential simulations.
		Lastly, a generic inter-operator has been established, which, subject
			to further development, promises to significantly ease the
			recycling of groundwater data into the simulation codes of the
			future.
%		In short, an accessible and portable workflow of the process of
%			up-conversion between MODFLOW versions now avails itself to
%			geoscientists.
%		Some components of it may find applicability in the re-use of legacy
%			simulations.
%		Lastly, a generic inter-operator has been established, invoking the
%			possibility of significant ease in the recycling of groundwater
%			data in the future.

		Of~course, my very~presence at the TACC owes itself in~no~small~part to
			a generous stipend provided as part of a fellowship received with
			the
			\textit{{T}exas~{I}nstitute for {D}iscovery~{E}ducation in {S}cience}
			of the {C}ollege of {N}atural~{S}ciences of the
			{U}niversity of {T}exas at {A}ustin.
		To the donors whose kind~hearts replenish it and the administrators
			whose fastidiousness maintains it, I am extraordinary grateful to
			you all.
		Lastly, no work of mine could have been possible had it not been for
			those of \textit{{N}ational~{S}cience {F}oundation} who
			have shared in the vision of {IS}-{GEO} and magnanimously funded
			this {R}esearch~{C}oordination {N}etwork with award 1633221.

